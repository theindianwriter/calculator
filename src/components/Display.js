import React from 'react';
import './displayStyle.css';

class Display extends React.Component{
    state={ show: "0",
    };
    componentDidUpdate(prevProps){
        
        if(prevProps.showValue !== this.props.showValue)
            this.showScreen();
    }
    showScreen(){
             
            this.setState(()=> {
            return {show: this.props.showValue};
            });
            
        }
    
    
    
    
    
    render(){
        
        return(
            <div className='displayScreen'>
               <h1> {this.state.show} </h1>
            </div>


        );
    }


}

export default Display;