import React from 'react';
import './buttonStyle.css';

class Button extends React.Component{
render(){
    return(
        <button  onClick={this.props.handleClick}>
            {this.props.value}
        </button>
    );
}


}


export default Button;