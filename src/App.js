import React, { Component } from 'react';

import './App.css';
import Button from './components/Button';
import Display from './components/Display';





const buttons = { 
1: "1",
2: "2",
3: "3",
4: "+",
5: "4",
6: "5",
7: "6",
8: "-",
9: "7",
10: "8",
11: "9",
12: "*",
13: "0",
14: ".",
15: "=",
16: "^",
17: "Reset",
18: "Del",
19: "%",
20:"/",
}
class App extends Component {
  state={buttons,
    showValue: ""
    };
  
  reset=()=>{
    this.setState(()=>{
      return {showValue: ""};
    });
  }

  delete=()=>{
    this.setState((prevState)=>{
        let newShowValue = prevState.showValue.slice(0,prevState.showValue.length - 1)
        return {showValue: newShowValue};
    });
  }
  
  answer=()=>{
    this.setState(prevState=>{
      if(prevState.showValue.length){
      try{
           return{showValue: String(eval(prevState.showValue))};
       }
      catch(e){
         if(e instanceof SyntaxError)
            alert("Syntax Error: Try Another Expression");
          else if(e instanceof TypeError)
            alert("Math Error");
          else if(e instanceof EvalError)
            alert("Eval Error");
            else{
              alert("some error");
            }
         }
      }
    });
  }



  handleClicked=(id)=>{
      if(id==="17"){
        this.reset();
      }
      else if(id==="18"){
        this.delete();   
    }else if(id==="15"){
      this.answer();
    }
      else{ 
          this.setState(prevState=>{
              return{showValue: prevState.showValue+buttons[id]};
        });
      }

  }
  
  
  render() {
    
    return (
      <div className="App">
        <Display showValue={this.state.showValue} />
         <div className="buttonHolder">
         {
           Object.keys(this.state.buttons).map((id)=>(
             
              <Button value={this.state.buttons[id]} handleClick={()=>this.handleClicked(id)} />
           ))
        }
        </div>
      </div>
    );
  }
}

export default App;
